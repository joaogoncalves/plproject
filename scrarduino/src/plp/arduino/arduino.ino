#include <Servo.h>

int START_DELAY  = 200;
int DELAY  = 1000;

int MOVER_DELAY = 100;
double GIRAR_DELAY = 8.5;

int DIRECAO_PARAFRENTE =  100;
int DIRECAO_PARATRAS   = -100;

int PIN_RODA1 = 9;
int PIN_RODA2 = 10;

Servo motor1;   
Servo motor2;   

boolean executar = true;

void setup() {
  
}

void loop() {
    
  if(executar) {
	moveXStep(100);
	espera(2);
	turnXDegreesRigth(90);
	turnXDegreesLeft(90);
	led(true,13);
	espera(1);
	led(false,13);
	espera(5);
	led(true,13);
	espera(1);
	led(false,13);
	espera(5);
	led(true,13);
	espera(1);
	led(false,13);
	espera(5);
	led(true,13);
	espera(1);
	led(false,13);
	espera(5);
	led(true,13);
	espera(1);
	led(false,13);
	espera(5);
	//@ACAO@
    desativar();
  }
}

void ligarMotores() {
  motor1.attach(PIN_RODA1); 
  motor2.attach(PIN_RODA2); 
}

void desligarMotores() {
  motor2.detach();
  motor1.detach();
}

void mover(int direcao, int tempo) {
  ligarMotores();
  motor1.write(direcao);
  motor2.write(direcao * 0.93);
  delay(MOVER_DELAY * tempo);
  desligarMotores();
  delay(DELAY);
}

void girar(int direcao1, int direcao2, int tempo) {
  ligarMotores();
  motor1.write(direcao1);
  motor2.write(direcao2 * 0.93);
  delay(GIRAR_DELAY * tempo);
  desligarMotores();
  delay(DELAY);
}

void desativar() {
  executar = false;
  desligarMotores();
}

void moveXStep(int valor) {
  mover(DIRECAO_PARAFRENTE, valor);
}


void turnXDegreesLeft(int valor) {
  girar(DIRECAO_PARATRAS, DIRECAO_PARAFRENTE, valor); 
}

void turnXDegreesRigth(int valor) {
  girar(DIRECAO_PARAFRENTE, DIRECAO_PARATRAS, valor); 
}

void led(boolean estado, int pin) {
 pinMode(pin, OUTPUT);
 if (estado) {
  digitalWrite(pin, HIGH);
 } else {
  digitalWrite(pin, LOW);
 } 
}

void espera(int tempo) {
  delay(tempo * DELAY); // Converter em x(tempo) segundos 
}
