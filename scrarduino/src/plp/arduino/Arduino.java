package plp.arduino;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Arduino {

	private static String templatePath = null;
	private static String codePath = null;

	private static String getPath(String path) {
		String retorno = null;

		try {
			retorno = new File(path).getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return retorno;
	}

	private static String getTemplatePath() {

		if (templatePath != null) {
			return templatePath;
		} else {
			return getPath("../scrarduino/src/plp/arduino/template.ino");
		}

	}

	private static String getCodePath() {

		if (codePath != null) {
			return codePath;
		} else {
			return getPath("../scrarduino/src/plp/arduino/arduino.ino");
		}

	}

	private static void escreverAcao(String acao) {

		try {
			StringBuilder string = new StringBuilder();

			BufferedReader in = new BufferedReader(
					new FileReader(getCodePath()));
			while (in.ready()) {
				String linha = in.readLine();

				if (linha.contains("@ACAO@")) {
					string.append("\t");
					string.append(acao);
					string.append("\n");
					string.append("\t//@ACAO@");
				} else {
					string.append(linha);
				}
				string.append("\n");
			}
			in.close();

			BufferedWriter out = new BufferedWriter(new FileWriter(
					getCodePath()));
			out.write(string.toString());
			out.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void inicializar() {

		try {

			String templatePath = getTemplatePath();
			String codePath = getCodePath();

			File arquivoCodigo = new File(codePath);

			if (arquivoCodigo.exists()) {
				arquivoCodigo.delete();
			}

			arquivoCodigo.createNewFile();

			BufferedReader in = new BufferedReader(new FileReader(templatePath));
			BufferedWriter out = new BufferedWriter(new FileWriter(codePath));

			String linha;

			while (in.ready()) {
				linha = in.readLine();

				out.write(linha);
				out.write("\n");
			}

			
			in.close();
			out.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void andar(int valor) {
		
		escreverAcao("moveXStep(" + valor + ");");
	}

	public static void girarEsquerda(int valor) {
		escreverAcao("turnXDegreesLeft(" + valor + ");");
	}

	public static void girarDireita(int valor) {
		escreverAcao("turnXDegreesRigth(" + valor + ");");
	}

	public static void usarLed(boolean valor, int pin) {
		escreverAcao("led(" + valor + "," + pin + ");");
	}

	public static void aguardar(int tempo) {
		escreverAcao("espera(" + tempo + ");");
	}

}
