package plp.imperative2.expression;

import plp.expressions1.util.Tipo;
import plp.expressions1.util.TipoPrimitivo;
import plp.expressions2.expression.ExpBinaria;
import plp.expressions2.expression.Expressao;
import plp.expressions2.expression.Valor;
import plp.expressions2.expression.ValorBooleano;
import plp.expressions2.expression.ValorInteiro;
import plp.expressions2.memory.AmbienteCompilacao;
import plp.expressions2.memory.AmbienteExecucao;
import plp.expressions2.memory.VariavelJaDeclaradaException;
import plp.expressions2.memory.VariavelNaoDeclaradaException;

public class ExpressaoMenorQue extends ExpBinaria {

	public ExpressaoMenorQue(Expressao esq, Expressao dir) {
		super(esq, dir, "<");
	}

	@Override
	public Valor avaliar(AmbienteExecucao ambiente) throws VariavelNaoDeclaradaException, VariavelJaDeclaradaException {
		ValorInteiro valorEsquerda = (ValorInteiro) esq.avaliar(ambiente);
		ValorInteiro valorDireita = (ValorInteiro) dir.avaliar(ambiente);

		return new ValorBooleano(valorEsquerda.valor() < valorDireita.valor());
	}


	@Override
	public Tipo getTipo(AmbienteCompilacao amb)
			throws plp.expressions2.memory.VariavelNaoDeclaradaException,
			plp.expressions2.memory.VariavelJaDeclaradaException {
		return TipoPrimitivo.BOOLEANO;
	}

	@Override
	protected boolean checaTipoElementoTerminal(
			plp.expressions2.memory.AmbienteCompilacao amb)
			throws plp.expressions2.memory.VariavelNaoDeclaradaException,
			plp.expressions2.memory.VariavelJaDeclaradaException {
		return (esq.getTipo(amb).eInteiro() )
				&& (dir.getTipo(amb).eInteiro() );
		
		//return (esq.getTipo(amb).eInteiro() || esq.getTipo(amb).isParametro())
		// && (dir.getTipo(amb).eInteiro() || dir.getTipo(amb).isParametro());
	}

	@Override
	public ExpBinaria clone() {
		// TODO Auto-generated method stub
		return new ExpressaoMenorQue(esq.clone(), dir.clone());
	}
}