package plp.imperative2.declaration;

import plp.expressions1.util.Tipo;
import plp.expressions1.util.TipoPrimitivo;
import plp.expressions2.expression.ValorConcreto;
import plp.expressions2.memory.AmbienteCompilacao;

/**
 * Este valor primitivo encapsula um valor booleano.
 */
public class ValorEstado extends ValorConcreto<Boolean>{

	
	/**
	 * Cria um objeto encapsulando o valor booleano fornecido.
	 */
	public ValorEstado(boolean valor) {
		super(valor);
	}

	
	/**
	 * Retorna os tipos possiveis desta expressao.
	 *
	 * @param amb o ambiente de compilação.
	 * @return os tipos possiveis desta expressao.
	 */
	public Tipo getTipo(AmbienteCompilacao amb) {
		return TipoPrimitivo.ESTADO;
	}
		
	public ValorEstado clone() {
		return new ValorEstado(this.valor());
	}
}