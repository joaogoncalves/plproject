package plp.imperative2.command;

import plp.arduino.Arduino;
import plp.expressions2.expression.Expressao;
import plp.expressions2.expression.ValorInteiro;
import plp.expressions2.memory.IdentificadorJaDeclaradoException;
import plp.expressions2.memory.IdentificadorNaoDeclaradoException;
import plp.imperative1.memory.AmbienteCompilacaoImperativa;
import plp.imperative1.memory.AmbienteExecucaoImperativa;
import plp.imperative1.memory.EntradaVaziaException;
import plp.imperative1.memory.ErroTipoEntradaException;

public class MoveParaFrente extends Movimento {

	private static final String acao = "moveXStep";

	public MoveParaFrente(Expressao expressao) {
		super(expressao, acao);
	}

	@Override
	public AmbienteExecucaoImperativa executar(
			AmbienteExecucaoImperativa ambiente)
			throws IdentificadorJaDeclaradoException,
			IdentificadorNaoDeclaradoException, EntradaVaziaException,
			ErroTipoEntradaException {
		ValorInteiro numeroPassos = (ValorInteiro) super.getValor().avaliar(ambiente);

		Arduino.andar(numeroPassos.valor());

		return ambiente;
	}

	@Override
	public boolean checaTipo(AmbienteCompilacaoImperativa ambiente)
			throws IdentificadorJaDeclaradoException,
			IdentificadorNaoDeclaradoException, EntradaVaziaException {
		// Olhar melhorias
		return this.getValor().getTipo(ambiente).eInteiro();
	}

}
