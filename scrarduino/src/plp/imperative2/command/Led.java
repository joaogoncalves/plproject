package plp.imperative2.command;

import plp.arduino.Arduino;
import plp.expressions1.util.TipoPrimitivo;
import plp.expressions2.expression.Expressao;
import plp.expressions2.expression.ValorInteiro;
import plp.expressions2.memory.IdentificadorJaDeclaradoException;
import plp.expressions2.memory.IdentificadorNaoDeclaradoException;
import plp.imperative1.command.Comando;
import plp.imperative1.memory.AmbienteCompilacaoImperativa;
import plp.imperative1.memory.AmbienteExecucaoImperativa;
import plp.imperative1.memory.EntradaVaziaException;
import plp.imperative1.memory.ErroTipoEntradaException;
import plp.imperative2.declaration.ValorEstado;
import plp.imperative2.memory.AmbienteExecucaoImperativa2;


public class Led implements Comando {

	private Expressao estado;
	private Expressao valor;

	public Led(Expressao estado, Expressao valor) {
		this.estado = estado;
		this.valor = valor;
	}

	public AmbienteExecucaoImperativa2 executar(
			AmbienteExecucaoImperativa ambiente)
			throws IdentificadorJaDeclaradoException,
			IdentificadorNaoDeclaradoException, EntradaVaziaException,
			ErroTipoEntradaException {

		ValorEstado estado = (ValorEstado) this.estado.avaliar(ambiente);
		ValorInteiro pin = (ValorInteiro) this.valor.avaliar(ambiente);

		Arduino.usarLed(estado.valor(), pin.valor());

		return (AmbienteExecucaoImperativa2) ambiente;
	}

	public boolean checaTipo(AmbienteCompilacaoImperativa ambiente)
			throws IdentificadorJaDeclaradoException,
			IdentificadorNaoDeclaradoException, EntradaVaziaException {
		return estado.checaTipo(ambiente)
				&& estado.getTipo(ambiente).eIgual(
						TipoPrimitivo.ESTADO) && valor.checaTipo(ambiente);
	}

}
