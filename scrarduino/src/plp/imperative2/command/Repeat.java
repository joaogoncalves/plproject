package plp.imperative2.command;

import plp.expressions2.expression.Expressao;
import plp.expressions2.expression.ValorBooleano;
import plp.expressions2.expression.ValorInteiro;
import plp.imperative1.command.Comando;
import plp.imperative1.memory.AmbienteExecucaoImperativa;
import plp.imperative1.memory.AmbienteCompilacaoImperativa;
import plp.imperative1.memory.EntradaVaziaException;
import plp.imperative1.memory.ErroTipoEntradaException;
import plp.expressions2.memory.IdentificadorJaDeclaradoException;
import plp.expressions2.memory.IdentificadorNaoDeclaradoException;

public class Repeat implements Comando {

	private Expressao expressao;
	private Comando comando;

	public Repeat(Expressao expressao, Comando comando) {
		this.expressao = expressao;
		this.comando = comando;
	}

	public AmbienteExecucaoImperativa executar(
			AmbienteExecucaoImperativa ambiente)
			throws IdentificadorJaDeclaradoException,
			IdentificadorNaoDeclaradoException, EntradaVaziaException, ErroTipoEntradaException {
		
		if(expressao.avaliar(ambiente)instanceof ValorBooleano){
			while (((ValorBooleano) expressao.avaliar(ambiente)).valor()) {
				ambiente = comando.executar(ambiente);
			}
		}else if(expressao.avaliar(ambiente)instanceof ValorInteiro){
			for (int i = 0; i < ((ValorInteiro) expressao.avaliar(ambiente)).valor(); i++) {
				ambiente = comando.executar(ambiente);
			}
		}
		
		return ambiente;
	}

	/**
	 * Realiza a verificacao de tipos da express�o e dos comandos do comando
	 * <code>while</code>
	 * 
	 * @param ambiente
	 *            o ambiente de compila��o.
	 * @return <code>true</code> se os comando s�o bem tipados;
	 *         <code>false</code> caso contrario.
	 */
	public boolean checaTipo(AmbienteCompilacaoImperativa ambiente)
			throws IdentificadorJaDeclaradoException,
			IdentificadorNaoDeclaradoException, EntradaVaziaException {
		return expressao.checaTipo(ambiente)
				&& expressao.getTipo(ambiente).eBooleano()
				&& comando.checaTipo(ambiente);
	}

}
