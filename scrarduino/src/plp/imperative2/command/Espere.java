package plp.imperative2.command;

import plp.arduino.Arduino;
import plp.expressions2.expression.Expressao;
import plp.expressions2.expression.ValorInteiro;
import plp.expressions2.memory.IdentificadorJaDeclaradoException;
import plp.expressions2.memory.IdentificadorNaoDeclaradoException;
import plp.imperative1.command.Comando;
import plp.imperative1.memory.AmbienteCompilacaoImperativa;
import plp.imperative1.memory.AmbienteExecucaoImperativa;
import plp.imperative1.memory.EntradaVaziaException;
import plp.imperative1.memory.ErroTipoEntradaException;

public class Espere implements Comando {

	private Expressao expressao;

	public Espere(Expressao expressao) {
		this.expressao = expressao;
	}

	public AmbienteExecucaoImperativa executar(AmbienteExecucaoImperativa ambiente)
			throws IdentificadorJaDeclaradoException,
			IdentificadorNaoDeclaradoException, EntradaVaziaException,
			ErroTipoEntradaException {

		ValorInteiro tempo = (ValorInteiro) expressao.avaliar(ambiente);
		
		Arduino.aguardar(tempo.valor());
		
		return ambiente;
	}

	public boolean checaTipo(AmbienteCompilacaoImperativa ambiente)
			throws IdentificadorJaDeclaradoException,
			IdentificadorNaoDeclaradoException, EntradaVaziaException {

		return expressao.getTipo(ambiente).eInteiro();
	}

}
