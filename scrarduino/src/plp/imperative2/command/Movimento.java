package plp.imperative2.command;

import plp.expressions2.expression.Expressao;
import plp.expressions2.expression.ValorInteiro;
import plp.expressions2.memory.IdentificadorJaDeclaradoException;
import plp.expressions2.memory.IdentificadorNaoDeclaradoException;
import plp.imperative1.command.Comando;
import plp.imperative1.memory.AmbienteCompilacaoImperativa;
import plp.imperative1.memory.AmbienteExecucaoImperativa;
import plp.imperative1.memory.EntradaVaziaException;
import plp.imperative1.memory.ErroTipoEntradaException;

public abstract class Movimento implements Comando {

	private Expressao expressao;
	private String acao;

	public Movimento(Expressao expressao, String acao) {
		super();
		this.expressao = expressao;
		this.acao = acao;
	}

	public abstract AmbienteExecucaoImperativa executar(
			AmbienteExecucaoImperativa ambiente)
			throws IdentificadorJaDeclaradoException,
			IdentificadorNaoDeclaradoException, EntradaVaziaException,
			ErroTipoEntradaException;

	public abstract boolean checaTipo(AmbienteCompilacaoImperativa ambiente)
			throws IdentificadorJaDeclaradoException,
			IdentificadorNaoDeclaradoException, EntradaVaziaException;

	public Expressao getValor() {
		return expressao;
	}

	public void setValor(ValorInteiro valor) {
		this.expressao = valor;
	}

	public String getAcao() {
		return acao;
	}

}
